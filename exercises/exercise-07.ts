import { Subject, bufferCount, concat, debounceTime, distinctUntilChanged, filter, interval, map, merge, of, reduce, scan, skip, startWith, take, withLatestFrom, zip } from '../lib/rxjs';
import { word$ } from '../lib/example-streams';
import { checkSolution } from '../lib/solution-checker/index';

// The word$ stream (imported above) will emit a sequence words. Each word is just a string.
//
// ASSIGNMENT: Concatenate all the words from the word$ stream to form a sentence. Emit the final and intermediate
// results (the partial sentences) via the sentence$ stream.

const sentence$ = word$; // ???

// If implemented correctly you should now know what "jirble" means.

checkSolution(sentence$);
