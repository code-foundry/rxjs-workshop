import { Observable, MonoTypeOperatorFunction, debounceTime as __debounceTime, interval as __interval } from 'rxjs';

export {
    Subject,
    bufferCount,
    concat,
    distinctUntilChanged,
    filter,
    map,
    max,
    merge,
    of,
    reduce,
    scan,
    skip,
    startWith,
    take,
    withLatestFrom,
    zip,
} from 'rxjs';

export { cache } from './cache.operator';

import { virtualTime } from './virtual-time-scheduler';

export function interval(period: number): Observable<number> {
    return __interval(period, virtualTime);
}

export function debounceTime<T>(dueTime: number): MonoTypeOperatorFunction<T> {
    return __debounceTime<T>(dueTime, virtualTime);
}
