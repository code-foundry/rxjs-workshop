import { MonoTypeOperatorFunction, publishReplay, refCount } from 'rxjs';

export function cache<T>(): MonoTypeOperatorFunction<T> {
    return (source$) => source$.pipe(
        publishReplay(),
        refCount(),
    );
}
