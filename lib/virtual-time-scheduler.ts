import { VirtualTimeScheduler } from 'rxjs';

export const virtualTime = new VirtualTimeScheduler();

export function setTimeout(callback: () => any, delay: number): void {
    virtualTime.schedule(callback, delay);
}
